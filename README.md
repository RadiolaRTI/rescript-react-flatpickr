# rescript-react-flatpickr #

>Rescript binding for React Flatpickr.

## Table of Contents

* [Installation](#markdown-header-installation)
* [Usage](#markdown-header-usage)
* [Examples](#markdown-header-examples)
* [Acknowledgements](#markdown-header-acknowledgement)

## Installation

Run the following in your project:
```console
npm install rescript-react-flatpicker --save
```

Then, add rescript-react-flatpickr in `bsconfig.json`:
```diff
-- "bs-dependencies": [],
++ "bs-dependencies": ["rescript-react-flatpickr"],
```

This package includes required peer dependencies and therefore is the only package required to use flatpickr in your own rescript project.


## Usage

Because accepting different types for a single prop is not type safe these bindings use polymorphic
variants instead to represent each one of the possible types:

```rescript
@react.component
let make = () =>
  <Flatpickr value=#date(Js.Date.now()) />;
```

### Basic props

All props are optional

#### defaultValue

`string` This will be passed to the inner input

#### value

This can either be `string`, `array(string)`, `Js.Date.t` or `array(Js.Date.t)` you need to use
polymorphic variants for these to be typesafe:

- *#str* - When you need values to be a ```string```
- *#strs* - When you need values to be ```array(string)```
- *#date* - When you need Values to be ```Js.Date.t```
- *#dates* - When you need values to be ```array(Js.Date.t)```

You use them by wrapping your value in the right variant like this:

```rescript
<Flatpickr value=#str("2020-06-12") />
```
#### options

`Js.t({..})` we provide a function that will help you generate these options for you `FlatpickrOptions.make`
all argument are optional and those not set will be set to `flatpickr.js` defaults.

- `Flatpickr options`: you can pass all `Flatpickr parameters` here.
- All `Flatpickr` [hooks][hooks] can be passed within this option too.

_*Example*_:

```rescript
@react.component
let make = () => {
  let today = Js.Date.now();
  let daysFromNow = today -> Js.Date.fromFloat

  <Flatpickr
    options={
      FlatpickrOptions.make(
        ~dateFormat="l, d/m/Y",
        ~maxDate=#date(daysFromNow),
        (),
      )}
      value=today
  />;
};
```

#### children

`React.element` this prop is closely related with the [wrap option](https://flatpickr.js.org/examples/#flatpickr-external-elements) 
from `Flatpickr`, please refer to the former link for more information.

#### className

`string` This class will be applied to the inner `<input />` element.

### Event handlers

`(Value.t, string) => unit` every event handler prop has this type.

- `Value.t` - first arugment is an `array(Js.Date.t)` representing selected dates, if
no dates have been selected array will be empty.
- `string` - second argument is a representation of the latest selected date as a string
formatted by the `dateFormat` (see options section).

The following `props` are provided in order to customize the `Flatpickr's functions` default behaviour.
Please refer to the [Events & Hooks section](https://flatpickr.js.org/events/) from `Flatpickr` library.

**Note:** Event handlers for `flatpickr.js` have a third argument which is a `flatpickr` instance we've
ommitted that since the idea is to handle everything via react, that said if a valid usecase for it arises
we can add it in the future.

- `onChange`
- `onClose`
- `onDayCreate`
- `onDestroy`
- `onMonthChange`
- `onOpen`
- `onReady`
- `onValueUpdate`

### Styling

Just download any of the already present `flatpickr.js` themes and add a reference in `index.html`

```html
<!doctype html>
<html lang="en">
<head>
  <link rel="stylesheet" href="../assets/css/flatpickr.min.css">
</head>
...
```

**Note:** Both `FlatpickrTypes.Hooks` and `FlatpickrTypes.Value` have a `classify` function for boxing
and a `reduce` function for `unboxing`. This is because both the [value](#value) and [options](#options)
prop can be of more than one type:

```rescript
@react.component
let make = () => {
  let (date, setDate) = React.useState(_ => #date(Js.Date.now()));
  FlatpickrTypes.(
    <Flatpickr
      value=date
      onChange={
        (value, _) => setDate(_ => Value.classify(value))
      }
    />
  );
};
```

## Acknowledgements

Flatpickr: https://github.com/flatpickr/flatpickr

React Flatpickr: https://github.com/haoxins/react-flatpickr

Bs React Flatpickr: https://github.com/cloverinteractive/bs-react-flatpickr